﻿
using System.Collections.Generic;

using System.Web.Mvc;

namespace MyUmbraco.Models
{
    public class YpologismosDMSViewModel
    {
        public string Gender { get; set; }

        public decimal Age { get; set; }

        public decimal Height { get; set; }

        public decimal Weight { get; set; }
    }
}