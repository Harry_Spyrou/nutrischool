﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyUmbraco.Models
{
    public class SearchBarViewModel
    {
        public string query { get; set; }
       
        public int[] NodeIdArray { get; set; }
    }
}