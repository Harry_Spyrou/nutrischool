﻿using System.Web.Mvc;
using MyUmbraco.Models;
using Umbraco.Web.Mvc;

namespace MyUmbraco.Controllers
{
    public class SearchSurfaceController : SurfaceController
    {

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleTheSearching(SearchBarViewModel model)
        {
            if (!string.IsNullOrEmpty(model.query))
            {
                var query = Request.QueryString["query"];
                //var url = Umbraco.Content("").Url;
                return new RedirectResult("/SearchResultsPage/" + "?query=" + model.query);//This MUST return /SearchResultsPage?query="query"(second query is the user input)

            }
        
            else 
            {
                return new RedirectResult("/", false);
            }
        }

    }
}
        



    