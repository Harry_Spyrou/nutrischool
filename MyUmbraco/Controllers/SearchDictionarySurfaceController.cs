﻿using MyUmbraco.Models;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using System;

namespace MyUmbraco.Controllers
{
    public class SearchDictionarySurfaceController : SurfaceController
    {

        // GET: SearchDictionarySurface
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult HandleDictionarySearching(string query)
        {
 
            if (!string.IsNullOrEmpty(query))
             {
                
                List<int> ids = new List<int>();
                foreach (var item in Umbraco.TypedContent(1063).Descendant("diatrofikoLeksiko").Children)
                {
                    string compareToQuery = item.GetPropertyValue<string>("Leksi");
                    if (compareToQuery.ToLower().Contains(query.ToLower().ToString()))
                    {
                        ids.Add(item.Id);
                        
                    }
                    else
                    {
                        continue;
                    }
                }


                SearchBarViewModel model = new SearchBarViewModel();
                model.query = query;
                model.NodeIdArray = ids.ToArray();
                int[] NodeIds = model.NodeIdArray;
                
               
                return Json(NodeIds);
            }
            
            else
            {
                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Home"),
                    isRedirect = true
                });
            }


        }
    }
}