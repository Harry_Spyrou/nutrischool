﻿using MyUmbraco.Models;
using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace MyUmbraco.Controllers
{
    public class NewsletterSurfaceController : SurfaceController
    {
        // GET: NewsletterSurface
        public bool validateEmail(string nEmail)
        {
            string filter = @"^(?i:(?<local_part>[a-z0-9!#$%^&*{}'`+=-_|/?]+(?:\.[a-z0-9!#$%^&*{}'`+=-_|/?]+)*)@(?<labels>[a-z0-9]+\z?.*[a-z0-9-_]+)*(?<tld>\.[a-z0-9]{2,}))$";
            return System.Text.RegularExpressions.Regex.IsMatch(nEmail, filter);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleNewsletterForm(ContactFormViewModel model)
        {

            #region Check Attributes

            if (string.IsNullOrWhiteSpace(model.Email))
            {
                ModelState.AddModelError("Email", "Παρακαλώ πληκτρολογήστε το email σας");
            }
            #endregion

            //########################################################################################//

            if ((!ModelState.IsValid))
            {
                return CurrentUmbracoPage();
            }

            else
            {
                #region Email

                try
                {
                    #region Build Email Body

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("<table>");

                    sb.Append("<tr>");
                    sb.Append("<td>Email:</td>");
                    sb.Append("<td>&nbsp;" + model.Email + "</td>");
                    sb.Append("</tr>");

                    sb.Append("</table>");
                    sb.Append("<p>&nbsp;</p>");
                    sb.Append("<p>&nbsp;</p>");
                    #endregion

                    //################################################################//

                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                    msg.IsBodyHtml = true;
                    msg.Body = sb.ToString();
                    msg.ReplyToList.Add(new System.Net.Mail.MailAddress(model.Email, model.Name + " " + model.Surname));
                    msg.Subject = "Newsletter";
                    msg.BodyEncoding = System.Text.Encoding.UTF8;
                    msg.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailSmtpFrom"],
                        System.Configuration.ConfigurationManager.AppSettings["MailSmtpDisplayName"]);

                    msg.To.Add(System.Configuration.ConfigurationManager.AppSettings["Recipients"]);
                    if (System.Configuration.ConfigurationManager.AppSettings["CcRecipients"] != "")
                    {
                        msg.CC.Add(System.Configuration.ConfigurationManager.AppSettings["CcRecipients"]);
                    }

                    if (System.Configuration.ConfigurationManager.AppSettings["BccRecipients"] != "")
                    {
                        msg.Bcc.Add(System.Configuration.ConfigurationManager.AppSettings["BccRecipients"]);
                    }

                    #region Send Email

                    System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                    client.Port = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["MailSmtpPort"]);
                    client.Host = System.Configuration.ConfigurationManager.AppSettings["MailSmtpHost"];
                    client.EnableSsl = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["MailUseSsl"]);
                    client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    client.Credentials = new System.Net.NetworkCredential(
                    System.Configuration.ConfigurationManager.AppSettings["MailSmtpUsername"],
                    System.Configuration.ConfigurationManager.AppSettings["MailSmtpPassword"]);

                    client.Send(msg);

                    #endregion

                    string success = "true";
                    return new RedirectResult(CurrentPage.Url + "?send=" + success, false);

                }
                catch (Exception)
                {
                    string success = "false";
                    return new RedirectResult(CurrentPage.Url + "?send=" + success, false);
                }
                #endregion
            }
        }
    }
}
    