﻿using MyUmbraco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace MyUmbraco.Controllers
{
    
    public class HomePageLexiconSurfaceController : SurfaceController
    {
        [HttpPost]
        // GET: HomePageLexiconSurface
        public ActionResult HomePageLexicon(SearchBarViewModel model)
        {
            if (!string.IsNullOrEmpty(model.query))
            {
                var query = Request.QueryString["query"];
                //var url = Umbraco.Content("").Url;
                return new RedirectResult("/diatrofiko-leksiko/" + "?query=" + model.query);//This MUST return /SearchResultsPage?query="query"(second query is the user input)

            }
            else
            {
                return new RedirectResult("/", false);
            }
        }
    }
}