﻿

    $(document).ready(function () {

        //ola ta node ids twn leksewn pou uparxoun sto lexiko se strings||| to do: int array from string array
        var IDs = [];
        $("#dictionary").find(".styled").each(function(){ IDs.push(this.id); });
        var intIdArray = [];//number array of all node ids
        for (i = 0; i < IDs.length; i++)
        {
            var nodeId2 = parseInt(IDs[i], 10);           
            intIdArray.push(nodeId2);

            //TO DO: change the original array to keep the new int values. Done: created a new array and put the values in it. Works.
        }

        query2 = $("#query").val()

        function ajaxCall() {
            event.preventDefault();
            // Get some values from elements on the page:
            var $form = $(this),

            url = "/umbraco/Surface/SearchDictionarySurface/HandleDictionarySearching/";

            var posting = $.post(url, { query: query2 })
            // node Ids pou mas epistrefei o controller (query results)
            var nodeIds = [];
            var lastArray = [];


            posting.done(function (data) {

                // for loop gia na ginei to json =>number array. Works.
                for (i = 0; i < data.length; i++) {
                    var nodeIdInt = JSON.parse(data[i])
                    nodeIds.push(nodeIdInt);
                }

                //compare the elements of the two arrays. the identical ones will go into a third array named lastArray. Works.
                for (i = 0; i < intIdArray.length; i++) {
                    if (nodeIds.includes(intIdArray[i])) {
                        lastArray.push(intIdArray[i]);
                    }
                    else {
                        continue;
                    }
                };

                //if a node id of the lastArray matches an actual id in a div and the val of the query is not null, then display: block or else display: none;
                $("#dictionary").find(".styled").each(function (index, element) {
                    var dictionaryItem = $(element);

                    if (lastArray.includes(parseInt(dictionaryItem.attr('id')))) {
                        $(dictionaryItem).css("display", "block");

                    }
                    else if (!lastArray.includes(parseInt(dictionaryItem.attr('id'))) && $("#query").val() !== "") {
                        $(dictionaryItem).css("display", "none");
                    }
                    else {
                        $("#dictionary").find(".styled").each(function (index, element) {
                            $(dictionaryItem).css("display", "block");
                            alert("null!");
                        });
                    }
                });
            }); // posting.done end
            return false;
        }

        
        if (query2 != null)
        {
            ajaxCall();
        }
        $("#searchForm").keyup(function (event) {
            ajaxCall();

        });
        
        // Attach an onkeyup submit handler to the form
        
       
            $(window).keydown(function (event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    return false;
                }
            });
    });